# KETI SYNTHETIC DATASET

DATASET sample link:  
\#01: https://www.dropbox.com/t/C2EWHA7zanE1H5qS  
\#02: https://www.dropbox.com/t/CzQj4xbqmerIDPCp  
\#03: https://www.dropbox.com/t/tKzGV5iRT8CMS0mk  
\#04: https://www.dropbox.com/t/tSO07a7mm5KpPMnW  
\#05: https://www.dropbox.com/t/v29UzjhvD13Kka0P  
\#06: https://www.dropbox.com/t/YYhNGX1Hy2vPDNqJ  
\#07: https://www.dropbox.com/t/zI4tTddYIdRqHUOB  
\#08: https://www.dropbox.com/t/Rmpo2N2cwVKllCJs  
\#09: https://www.dropbox.com/t/XYJsPTEeHDjwpfZS  
\#10: https://www.dropbox.com/t/wqDOeucJB3egF7hJ  
\#11: https://www.dropbox.com/t/u5aNX6OVSPZ6kC4s  
\#12: https://www.dropbox.com/t/GOQvLqmZhgPuBDpN  
\#13: https://www.dropbox.com/t/xpZt8f13v8TOCP4L  
\#14: https://www.dropbox.com/t/cpNXjBEQavuoiHbw  

## 소개

KETI SYNTHETIC DATASET은 자율주행 기술에 필수적으로 사용되는 인공지능 기반의 자율주행 기술에 활용 가능한 인공지능 학습 및 검증용 데이터셋을 시뮬레이터상의 가상의 도로 주행환경에서 생성하여 GT(Ground Truth) 값과 함께 제공하는 것을 목표로 합니다.

이를 위해 한국의 도로 환경을 가상의 컨텐츠에 동일하게 구성하고 시뮬레이터 상의 자율주행 차량 주변에 대한 다양한 상황을 모사하고 있습니다. 인공지능 알고리즘의 학습/검증에 활용할 수 있도록 시뮬레이터 내의 자율주행용 차량에 가상의 센서를 장착하고 센서 데이터를 취득하고 이에 대한 GT 데이터를 생성하여 제공합니다.

KETI SYNTHETIC DATASET 벤치마크의 목표는 다음과 같습니다.

- 국내 도로 환경의 가상 세계(컨텐츠) 구축을 통한 국내 상황에 맞는 인공지능 학습 지원
- 인공지능 기반의 자율주행을 위한 인지, 판단, 제어 알고리즘의 학습/검증에 필요한 합성 데이터셋 구축
- 가상 카메라 센서 기반의 DETECTION과 SEGMENTATION 학습/검증 테스트 데이터 제공
- 가상 라이다 센서 기반의 DETECTION용 데이터셋 제공

### Feature

- KETI Synthetic Dataset은 대한민국 도심 및 시골의 데이터입니다.
- 다양한 포맷의 데이터셋을 제공합니다. (2D/3D Detection, Segmentation, Etc)
- 데이터 분석/검증을 위한 다양한 속성값이 존재합니다. (Scene Attribute, Object Attribute)

1. 카메라 영상정보 기반의 데이터셋은 Raw 데이터와 두 가지 타입에 대한 Ground Truth Annotation을 제공합니다.  
    ① Object Detection : Bounding Box 형태의 Ground Truth Annotation 제공  
    ② Semantic-segmentation : Image 형태의 Ground Truth Annotation 제공  
2. 라이다 데이터셋은 Raw 데이터와 3D Bounding Box 형태의 Ground Truth Annotation을 제공합니다.
3. 데이터셋 취득은 다음과 같이 다양한 환경에서 취득되었습니다.  
    ① 데이터 취득 장소  
      : 서울특별시 상암 DMC  
    ② 데이터 취득 환경  
      : 주간, 야간, 우천 등  

## DATASET 구조

KETI DATASET은 아래와 같은 구조로 구성되어 있습니다.

![Untitled](/uploads/ad41e725b7240afefff49522a54f9922/dataset_directory_structure.png)

- 각각의 구성 요소는 다음을 포함합니다.
- KETI Dataset
    - license.txt(라이선스)
    - README(데이터셋 및 배포 관련 정보 포함)
    - Camera (raw data)
    - Lidar (raw data)
    - Annotation (Ground Truth data)
        - Detection
            Bounding-box 형태의 GT 정보(class id, 2D/3D bounding box 좌표 등)
        - Segmentation
            Semantic Segmentation GT (2D 이미지)
            

## KETI TEAM

![Untitled](/uploads/a3915cfea97d7742d0d73047f8c7cf7c/dataset_keti_team.png) 
